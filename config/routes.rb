VcsRiskTool::Application.routes.draw do
  root 'welcome#index'

  devise_for :users

  resources :users do
	  resources :risks
	end

	resources :risks

	get '/about', to: 'welcome#about'

end