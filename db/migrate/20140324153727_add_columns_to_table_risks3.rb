class AddColumnsToTableRisks3 < ActiveRecord::Migration
  def change
    add_column :risks, :t10mf_total, :float
		add_column :risks, :t10mpd_total, :float
		add_column :risks, :t10mw_total, :float
		add_column :risks, :t10mg_total, :float
		add_column :risks, :t10mon_total, :float
  end
end
