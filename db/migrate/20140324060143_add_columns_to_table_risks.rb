class AddColumnsToTableRisks < ActiveRecord::Migration
  def change
    add_column :risks, :t4_pl_a, :integer
    add_column :risks, :t4_pl_b, :integer
    add_column :risks, :t6_lt_total, :integer
  end
end
