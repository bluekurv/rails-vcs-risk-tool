class ChangeColumnsOnTableRisks < ActiveRecord::Migration
  def change
  	remove_column :risks, :t1
  	change_column :risks, :t1a, :integer
		change_column :risks, :t1b, :integer
		change_column :risks, :t1c, :integer
		change_column :risks, :t1d, :integer
		change_column :risks, :t1e, :integer
		change_column :risks, :t1f, :integer
		change_column :risks, :t2a_d, :integer
		change_column :risks, :t2e_h, :integer
		change_column :risks, :t2i, :integer
		change_column :risks, :t3a_f, :integer
		change_column :risks, :t4_pl_value, :integer
		change_column :risks, :t4_selected, :integer
		change_column :risks, :t6a_b, :integer
		change_column :risks, :t6c, :integer
		change_column :risks, :t6d, :integer
		change_column :risks, :t6e, :integer
		change_column :risks, :t6f, :integer
		change_column :risks, :t6g, :integer
		change_column :risks, :t7a, :integer
		change_column :risks, :t7b, :integer
		change_column :risks, :t7c, :integer
		change_column :risks, :t8a_e, :integer
		change_column :risks, :t8f, :integer
  end
end
