class AddFieldsToRisks < ActiveRecord::Migration
  def change
  	change_table :risks do |t|
  		t.float :t1
  		t.float :t2a_d
  		t.float :t2e_h
  		t.float :t2i
  		t.float :t3a_f
  		t.float :t3g_i
  		t.float :t4_pl_value
  		t.float :t4_selected
  		t.float :t6a_b
  		t.float :t6c
  		t.float :t6d
  		t.float :t6e
  		t.float :t6f
  		t.float :t6g
  		t.float :t7a
  		t.float :t7b
  		t.float :t7c
  		t.float :t8a_e
  		t.float :t8f
  		t.float :t10la
  		t.float :t10lb
  		t.float :t10lc
  		t.float :t10ld
  		t.float :t10le
  		t.float :t10mf
  		t.float :t10mpd
  		t.float :t10mw
  		t.float :t10mg
  		t.float :t10mon
  		t.float :total1_4
  		t.float :total6_8
  		t.float :total10
  	end
  end
end
