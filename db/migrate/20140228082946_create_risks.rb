class CreateRisks < ActiveRecord::Migration
  def change
    create_table :risks do |t|
    	t.string :client
    	t.string :project
			t.integer :t1_pm_a
			t.integer :t1_pm_b
			t.integer :t1_pm_c
			t.integer :t1_pm_d
			t.integer :t1_pm_e
			t.integer :t1_pm_f
			t.integer :t1_pm_total
			t.integer :t2_fv_a
			t.integer :t2_fv_b
			t.integer :t2_fv_c
			t.integer :t2_fv_d
			t.integer :t2_fv_e
			t.integer :t2_fv_f
			t.integer :t2_fv_g
			t.integer :t2_fv_h
			t.integer :t2_fv_i
			t.integer :t2_fv_total
			t.integer :t3_oc_a
			t.integer :t3_oc_b
			t.integer :t3_oc_c
			t.integer :t3_oc_d
			t.integer :t3_oc_e
			t.integer :t3_oc_f
			t.integer :t3_oc_g
			t.integer :t3_oc_h
			t.integer :t3_oc_i
			t.integer :t3_oc_total
			t.integer :t4_pl_a
			t.integer :t4_pl_b
			t.integer :t4_pl_applic_a
			t.integer :t4_pl_applic_b
			t.integer :t4_pl_total
			t.integer :t5_total_internal_risk
			t.integer :t6_lt_a
			t.integer :t6_lt_b
			t.integer :t6_lt_c
			t.integer :t6_lt_d
			t.integer :t6_lt_e
			t.integer :t6_lt_f
			t.integer :t6_lt_total
			t.integer :t7_ce_a
			t.integer :t7_ce_b
			t.integer :t7_ce_c
			t.integer :t7_ce_total
			t.integer :t8_pr_a
			t.integer :t8_pr_b
			t.integer :t8_pr_c
			t.integer :t8_pr_d
			t.integer :t8_pr_e
			t.integer :t8_pr_f
			t.integer :t8_pr_total
			t.integer :t9_total_external_risk
			t.integer :table10_nr_part1_f_aa
			t.integer :table10_nr_part1_f_ab
			t.integer :table10_nr_part1_f_ac
			t.integer :table10_nr_part1_f_ad
			t.integer :table10_nr_part1_f_ae
			t.integer :table10_nr_part1_f_ba
			t.integer :table10_nr_part1_f_bb
			t.integer :table10_nr_part1_f_bc
			t.integer :table10_nr_part1_f_bd
			t.integer :table10_nr_part1_f_be
			t.integer :table10_nr_part1_f_ca
			t.integer :table10_nr_part1_f_cb
			t.integer :table10_nr_part1_f_cc
			t.integer :table10_nr_part1_f_cd
			t.integer :table10_nr_part1_f_ce
			t.integer :table10_nr_part1_f_cf
			t.integer :table10_nr_part1_f_da
			t.integer :table10_nr_part1_f_db
			t.integer :table10_nr_part1_f_dc
			t.integer :table10_nr_part1_f_dd
			t.integer :table10_nr_part1_f_de
			t.integer :table10_nr_part1_f_ea
			t.integer :table10_nr_part1_f_eb
			t.integer :table10_nr_part1_f_ec
			t.integer :table10_nr_part1_f_ed
			t.integer :table10_nr_part1_f_ee
			t.integer :table10_nr_part1_f_fa
			t.integer :table10_nr_part1_f_fb
			t.integer :table10_nr_part1_f_fc
			t.integer :table10_nr_part1_f_fd
			t.integer :table10_nr_part1_f_fe
			t.integer :table10_nr_part1_f_col_a_total
			t.integer :table10_nr_part1_f_col_b_total
			t.integer :table10_nr_part1_f_col_c_total
			t.integer :table10_nr_part1_f_col_d_total
			t.integer :table10_nr_part1_f_col_e_total
			t.integer :table10_nr_part1_f_col_f_total
			t.integer :table10_nr_part1_f_row_a_total
			t.integer :table10_nr_part1_f_row_b_total
			t.integer :table10_nr_part1_f_row_c_total
			t.integer :table10_nr_part1_f_row_d_total
			t.integer :table10_nr_part1_f_row_e_total
			t.integer :table10_nr_part1_f_row_f_total
			t.integer :table10_nr_part1_pd_aa
			t.integer :table10_nr_part1_pd_ab
			t.integer :table10_nr_part1_pd_ac
			t.integer :table10_nr_part1_pd_ad
			t.integer :table10_nr_part1_pd_ae
			t.integer :table10_nr_part1_pd_ba
			t.integer :table10_nr_part1_pd_bb
			t.integer :table10_nr_part1_pd_bc
			t.integer :table10_nr_part1_pd_bd
			t.integer :table10_nr_part1_pd_be
			t.integer :table10_nr_part1_pd_ca
			t.integer :table10_nr_part1_pd_cb
			t.integer :table10_nr_part1_pd_cc
			t.integer :table10_nr_part1_pd_cd
			t.integer :table10_nr_part1_pd_ce
			t.integer :table10_nr_part1_pd_cf
			t.integer :table10_nr_part1_pd_da
			t.integer :table10_nr_part1_pd_db
			t.integer :table10_nr_part1_pd_dc
			t.integer :table10_nr_part1_pd_dd
			t.integer :table10_nr_part1_pd_de
			t.integer :table10_nr_part1_pd_ea
			t.integer :table10_nr_part1_pd_eb
			t.integer :table10_nr_part1_pd_ec
			t.integer :table10_nr_part1_pd_ed
			t.integer :table10_nr_part1_pd_ee
			t.integer :table10_nr_part1_pd_fa
			t.integer :table10_nr_part1_pd_fb
			t.integer :table10_nr_part1_pd_fc
			t.integer :table10_nr_part1_pd_fd
			t.integer :table10_nr_part1_pd_fe
			t.integer :table10_nr_part1_pd_col_a_total
			t.integer :table10_nr_part1_pd_col_b_total
			t.integer :table10_nr_part1_pd_col_c_total
			t.integer :table10_nr_part1_pd_col_d_total
			t.integer :table10_nr_part1_pd_col_e_total
			t.integer :table10_nr_part1_pd_col_f_total
			t.integer :table10_nr_part1_pd_row_a_total
			t.integer :table10_nr_part1_pd_row_b_total
			t.integer :table10_nr_part1_pd_row_c_total
			t.integer :table10_nr_part1_pd_row_d_total
			t.integer :table10_nr_part1_pd_row_e_total
			t.integer :table10_nr_part1_pd_row_f_total
			t.integer :table10_nr_part1_w_aa
			t.integer :table10_nr_part1_w_ab
			t.integer :table10_nr_part1_w_ac
			t.integer :table10_nr_part1_w_ad
			t.integer :table10_nr_part1_w_ae
			t.integer :table10_nr_part1_w_ba
			t.integer :table10_nr_part1_w_bb
			t.integer :table10_nr_part1_w_bc
			t.integer :table10_nr_part1_w_bd
			t.integer :table10_nr_part1_w_be
			t.integer :table10_nr_part1_w_ca
			t.integer :table10_nr_part1_w_cb
			t.integer :table10_nr_part1_w_cc
			t.integer :table10_nr_part1_w_cd
			t.integer :table10_nr_part1_w_ce
			t.integer :table10_nr_part1_w_cf
			t.integer :table10_nr_part1_w_da
			t.integer :table10_nr_part1_w_db
			t.integer :table10_nr_part1_w_dc
			t.integer :table10_nr_part1_w_dd
			t.integer :table10_nr_part1_w_de
			t.integer :table10_nr_part1_w_ea
			t.integer :table10_nr_part1_w_eb
			t.integer :table10_nr_part1_w_ec
			t.integer :table10_nr_part1_w_ed
			t.integer :table10_nr_part1_w_ee
			t.integer :table10_nr_part1_w_fa
			t.integer :table10_nr_part1_w_fb
			t.integer :table10_nr_part1_w_fc
			t.integer :table10_nr_part1_w_fd
			t.integer :table10_nr_part1_w_fe
			t.integer :table10_nr_part1_w_col_a_total
			t.integer :table10_nr_part1_w_col_b_total
			t.integer :table10_nr_part1_w_col_c_total
			t.integer :table10_nr_part1_w_col_d_total
			t.integer :table10_nr_part1_w_col_e_total
			t.integer :table10_nr_part1_w_col_f_total
			t.integer :table10_nr_part1_w_row_a_total
			t.integer :table10_nr_part1_w_row_b_total
			t.integer :table10_nr_part1_w_row_c_total
			t.integer :table10_nr_part1_w_row_d_total
			t.integer :table10_nr_part1_w_row_e_total
			t.integer :table10_nr_part1_w_row_f_total
			t.integer :table10_nr_part1_g_aa
			t.integer :table10_nr_part1_g_ab
			t.integer :table10_nr_part1_g_ac
			t.integer :table10_nr_part1_g_ad
			t.integer :table10_nr_part1_g_ae
			t.integer :table10_nr_part1_g_ba
			t.integer :table10_nr_part1_g_bb
			t.integer :table10_nr_part1_g_bc
			t.integer :table10_nr_part1_g_bd
			t.integer :table10_nr_part1_g_be
			t.integer :table10_nr_part1_g_ca
			t.integer :table10_nr_part1_g_cb
			t.integer :table10_nr_part1_g_cc
			t.integer :table10_nr_part1_g_cd
			t.integer :table10_nr_part1_g_ce
			t.integer :table10_nr_part1_g_cf
			t.integer :table10_nr_part1_g_da
			t.integer :table10_nr_part1_g_db
			t.integer :table10_nr_part1_g_dc
			t.integer :table10_nr_part1_g_dd
			t.integer :table10_nr_part1_g_de
			t.integer :table10_nr_part1_g_ea
			t.integer :table10_nr_part1_g_eb
			t.integer :table10_nr_part1_g_ec
			t.integer :table10_nr_part1_g_ed
			t.integer :table10_nr_part1_g_ee
			t.integer :table10_nr_part1_g_fa
			t.integer :table10_nr_part1_g_fb
			t.integer :table10_nr_part1_g_fc
			t.integer :table10_nr_part1_g_fd
			t.integer :table10_nr_part1_g_fe
			t.integer :table10_nr_part1_g_col_a_total
			t.integer :table10_nr_part1_g_col_b_total
			t.integer :table10_nr_part1_g_col_c_total
			t.integer :table10_nr_part1_g_col_d_total
			t.integer :table10_nr_part1_g_col_e_total
			t.integer :table10_nr_part1_g_col_f_total
			t.integer :table10_nr_part1_g_row_a_total
			t.integer :table10_nr_part1_g_row_b_total
			t.integer :table10_nr_part1_g_row_c_total
			t.integer :table10_nr_part1_g_row_d_total
			t.integer :table10_nr_part1_g_row_e_total
			t.integer :table10_nr_part1_g_row_f_total
			t.integer :table10_nr_part1_on_aa
			t.integer :table10_nr_part1_on_ab
			t.integer :table10_nr_part1_on_ac
			t.integer :table10_nr_part1_on_ad
			t.integer :table10_nr_part1_on_ae
			t.integer :table10_nr_part1_on_ba
			t.integer :table10_nr_part1_on_bb
			t.integer :table10_nr_part1_on_bc
			t.integer :table10_nr_part1_on_bd
			t.integer :table10_nr_part1_on_be
			t.integer :table10_nr_part1_on_ca
			t.integer :table10_nr_part1_on_cb
			t.integer :table10_nr_part1_on_cc
			t.integer :table10_nr_part1_on_cd
			t.integer :table10_nr_part1_on_ce
			t.integer :table10_nr_part1_on_cf
			t.integer :table10_nr_part1_on_da
			t.integer :table10_nr_part1_on_db
			t.integer :table10_nr_part1_on_dc
			t.integer :table10_nr_part1_on_dd
			t.integer :table10_nr_part1_on_de
			t.integer :table10_nr_part1_on_ea
			t.integer :table10_nr_part1_on_eb
			t.integer :table10_nr_part1_on_ec
			t.integer :table10_nr_part1_on_ed
			t.integer :table10_nr_part1_on_ee
			t.integer :table10_nr_part1_on_fa
			t.integer :table10_nr_part1_on_fb
			t.integer :table10_nr_part1_on_fc
			t.integer :table10_nr_part1_on_fd
			t.integer :table10_nr_part1_on_fe
			t.integer :table10_nr_part1_on_col_a_total
			t.integer :table10_nr_part1_on_col_b_total
			t.integer :table10_nr_part1_on_col_c_total
			t.integer :table10_nr_part1_on_col_d_total
			t.integer :table10_nr_part1_on_col_e_total
			t.integer :table10_nr_part1_on_col_f_total
			t.integer :table10_nr_part1_on_row_a_total
			t.integer :table10_nr_part1_on_row_b_total
			t.integer :table10_nr_part1_on_row_c_total
			t.integer :table10_nr_part1_on_row_d_total
			t.integer :table10_nr_part1_on_row_e_total
			t.integer :table10_nr_part1_on_row_f_total
			t.integer :t10_part2_mit_a
			t.integer :t10_part2_mit_b
			t.integer :t10_part2_mit_c
			t.integer :t10_part2_mit_d
			t.integer :t10_part2_mit_e
			t.integer :t10_part2_mit_f
			t.integer :t10_part2_mit_a_total
			t.integer :t10_part2_mit_b_total
			t.integer :t10_part2_mit_c_total
			t.integer :t10_part2_mit_d_total
			t.integer :t10_part2_mit_e_total
			t.integer :t10_part2_mit_f_total
			t.integer :t10_part3_a_total
			t.integer :t10_part3_b_total
			t.integer :t10_part3_c_total
			t.integer :t10_part3_d_total
			t.integer :t10_part3_e_total
			t.integer :t10_part3_f_total
			t.integer :t10_part3_applic_f
  		t.integer :t10_part3_applic_pd
  		t.integer :t10_part3_applic_w
  		t.integer :t10_part3_applic_g
  		t.integer :t10_part3_applic_on
			t.integer :t10_total_natural_risk
			t.integer :overall_risk_rating
			t.references :user
      t.timestamps
    end
    add_index :risks, :user_id
  end
end
