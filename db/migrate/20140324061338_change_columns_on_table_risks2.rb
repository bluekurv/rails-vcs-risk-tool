class ChangeColumnsOnTableRisks2 < ActiveRecord::Migration
  def change
  	change_column :risks, :t6a_b, :float
  end
end
