class AddMoreFieldsToRisks < ActiveRecord::Migration
  def change
  	change_table :risks do |t|
  		t.float :t1a
  		t.float :t1b
  		t.float :t1c
  		t.float :t1d
  		t.float :t1e
  		t.float :t1f
  	end
  end
end
