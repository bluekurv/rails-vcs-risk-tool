# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140709050555) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "risks", force: true do |t|
    t.string   "client"
    t.string   "project"
    t.integer  "t1_pm_total"
    t.integer  "t2_fv_total"
    t.integer  "t3_oc_total"
    t.integer  "t5_total_internal_risk"
    t.integer  "t7_ce_total"
    t.integer  "t8_pr_total"
    t.integer  "t9_total_external_risk"
    t.integer  "t10_total_natural_risk"
    t.integer  "overall_risk_rating"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "t2a_d"
    t.integer  "t2e_h"
    t.integer  "t2i"
    t.integer  "t3a_f"
    t.float    "t3g_i"
    t.integer  "t4_pl_value"
    t.integer  "t4_selected"
    t.float    "t6a_b"
    t.integer  "t6c"
    t.integer  "t6d"
    t.integer  "t6e"
    t.integer  "t6f"
    t.integer  "t6g"
    t.integer  "t7a"
    t.integer  "t7b"
    t.integer  "t7c"
    t.integer  "t8a_e"
    t.integer  "t8f"
    t.float    "t10la"
    t.float    "t10lb"
    t.float    "t10lc"
    t.float    "t10ld"
    t.float    "t10le"
    t.float    "t10mf"
    t.float    "t10mpd"
    t.float    "t10mw"
    t.float    "t10mg"
    t.float    "t10mon"
    t.float    "total1_4"
    t.float    "total6_8"
    t.float    "total10"
    t.integer  "t1a"
    t.integer  "t1b"
    t.integer  "t1c"
    t.integer  "t1d"
    t.integer  "t1e"
    t.integer  "t1f"
    t.integer  "t4_pl_a"
    t.integer  "t4_pl_b"
    t.integer  "t6_lt_total"
    t.float    "t10_nr_part1_total"
    t.float    "t10mf_total"
    t.float    "t10mpd_total"
    t.float    "t10mw_total"
    t.float    "t10mg_total"
    t.float    "t10mon_total"
    t.integer  "t4except"
  end

  add_index "risks", ["user_id"], name: "index_risks_on_user_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
