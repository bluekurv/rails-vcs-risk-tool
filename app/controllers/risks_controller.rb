class RisksController < ApplicationController

	before_action :authenticate_user!

	#index, create, new, edit, show, update, destroy

	def index
		@risks = Risk.where(user_id: current_user.id)
	end

	def create
		@user = params[:user_id].blank? ? current_user : User.find(params[:user_id])
		@risk = @user.risks.create(params[:risk].permit!)
		#@risk = @user.risks.create(risk_params)

		if @risk.save
		 	redirect_to user_risk_path(@user,@risk)
		else
			redirect_to new_user_risk_path(@user), notice: "Client and Project must be filled."
		end
	end

	def new
		@risk = Risk.new
	end

	def edit
		@risk = Risk.find(params[:id])
	end

	def show
		@risk = Risk.find(params[:id])
	end

	def update
		@user = params[:user_id].blank? ? current_user : User.find(params[:user_id])
		@risk = Risk.find(params[:id])
		if @risk.update_attributes(params[:risk].permit!)
			redirect_to user_risks_path(@user)
		else
			redirect_to edit_user_risk_path(@user, @risk), notice: "Client and Project must be filled."
		end
	end

	def destroy
		@risk = Risk.find(params[:id])
		@risk.destroy
		redirect_to risks_path
	end

	private
		def risk_params
			params.permit(:t1a, :t1b)
		end

end
