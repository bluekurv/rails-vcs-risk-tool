class Risk < ActiveRecord::Base
	validates :client, :project, presence: true
	belongs_to :user
end
