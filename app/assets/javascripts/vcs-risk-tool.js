$( document ).ready(function() {

		//--- --- --- --- --- --- FORMULAS

		function table1calc() {
			var total1 = 0;
			$table1inputs.each(function() {
				if ($(this).is(":checked")) {
					total1 = parseInt(total1) + parseInt($(this).val());
				}
			});
			console.log('table 1 sum: ' + total1);
			$('#table1sum').val(total1);
		}
		function table2calc() {
			var total2 = 0;
			$table2inputs.each(function() {
				if ($(this).is(":checked")) {
					total2 = parseInt(total2) + parseInt($(this).val());
				}
			});
			console.log('table 2 sum: ' + total2);
			$('#table2sum').val(total2);
		}
		function table3calc() {
			var total3 = 0;
			$table3inputs.each(function() {
				if ($(this).is(":checked")) {
					total3 = parseInt(total3) + parseInt($(this).val());
				}
			});
			console.log('table 3 sum: ' + total3);
			$('#table3sum').val(total3);
		}
		function table6calc() {
			var total6 = 0;
			$table6inputs.each(function() {
				if ($(this).is(":checked")) {
					total6 = parseInt(total6) + parseInt($(this).val());
				}
			});
			console.log('table 6 sum: ' + total6);
			$('#table6sum').val(total6);
		}
		function table7calc() {
			var total7 = 0;
			$table7inputs.each(function() {
				if ($(this).is(":checked")) {
					total7 = parseInt(total7) + parseInt($(this).val());
				}
			});
			console.log('table 7 sum: ' + total7);
			$('#table7sum').val(total7);
		}
		function table8calc() {
			var total8 = 0;
			$table8inputs.each(function() {
				if ($(this).is(":checked")) {
					total8 = parseInt(total8) + parseInt($(this).val());
				}
			});
			console.log('table 8 sum: ' + total8);
			$('#table8sum').val(total8);
		}
		function table10part1calc() {
			var total10part1 = 0;
			$table10part1inputs.each(function() {
				if ($(this).is(":checked")) {
					total10part1 = parseInt(total10part1) + parseInt($(this).val());
				}
			});
			console.log('table 10 sum: ' + total10part1);
			$('#table10part1sum').val(total10part1);
		}

		//--- --- --- --- --- --- INTERACTIVITY ---//

		var $table1inputs = $('#table1 input').click(function (e) {
			table1calc();
		});
		var $table2inputs = $('#table2 input').click(function (e) {
			table2calc();
		});
		var $table3inputs = $('#table3 input').click(function (e) {
			table3calc();
		});
		var $table6inputs = $('#table6 input').click(function (e) {
			table6calc();
		});
		var $table7inputs = $('#table7 input').click(function (e) {
			table7calc();
		});
		var $table8inputs = $('#table8 input').click(function (e) {
			table8calc();
		});
		var $table10part1inputs = $('#table10part1 input').click(function (e) {
			table10part1calc();
		});

		//--- TABLE 4 OPTIONS---//
		$('#pl-normal').click(function() {
			document.getElementById('risk_t4_pl_a').disabled = false;
			document.getElementById('risk_t4_pl_b').disabled = false;
			document.getElementById('table4calculatebutton').style.display = "inline-block";
		});
		$('#pl-zero').click(function() {
			document.getElementById('risk_t4_pl_a').disabled = true;
			document.getElementById('risk_t4_pl_b').disabled = true;
			document.getElementById('table4calculatebutton').style.display = "none";
			$('#table4sum').val(0);
		});
		$('#pl-fail').click(function() {
			document.getElementById('risk_t4_pl_a').disabled = true;
			document.getElementById('risk_t4_pl_b').disabled = true;
			document.getElementById('table4calculatebutton').style.display = "none";
			$('#table4sum').val(99);
		});

		$('#table4calculatebutton').click(function() {
			var table4a = $('#risk_t4_pl_a').val();
			var table4b = $('#risk_t4_pl_b').val();
			var total4 = (24 - (table4a/5)) + (30 - (table4b/2));
			$('#table4sum').val(Math.round(total4));
		});

		$('#table10calculatebutton').click(function() {
			var t10part1 = $('#table10part1sum').val();
			var t10mf = $('#table10 input[name="risk\\[t10mf\\]"]:checked').val();
			var t10mpd = $('#table10 input[name="risk\\[t10mpd\\]"]:checked').val();
			var t10mw = $('#table10 input[name="risk\\[t10mw\\]"]:checked').val();
			var t10mg = $('#table10 input[name="risk\\[t10mg\\]"]:checked').val();
			var t10mon = $('#table10 input[name="risk\\[t10mon\\]"]:checked').val();

			var t10mf_total = t10part1 * t10mf;
			var t10mpd_total = t10part1 * t10mpd;
			var t10mw_total = t10part1 * t10mw;
			var t10mg_total = t10part1 * t10mg;
			var t10mon_total = t10part1 * t10mon;

			var t1 = parseInt($('#table1sum').val());
			var t2 = parseInt($('#table2sum').val());
			var t3 = parseInt($('#table3sum').val());
			var t4 = parseInt($('#table4sum').val());
			var t6 = parseInt($('#table6sum').val());
			var t7 = parseInt($('#table7sum').val());
			var t8 = parseInt($('#table8sum').val());

			var t5_total_internal_risk = t1 + t2 + t3 + t4;
			var t9_total_external_risk = t6 + t7 + t8;
			var t10_total_natural_risk = t10mf_total + t10mpd_total + t10mw_total + t10mg_total + t10mon_total;
			var overall_risk_rating = t5_total_internal_risk + t9_total_external_risk + t10_total_natural_risk;

			console.log(t4);

			$('#t10mf_total').val(Math.round(t10mf_total));
			$('#t10mpd_total').val(Math.round(t10mpd_total));
			$('#t10mw_total').val(Math.round(t10mw_total));
			$('#t10mg_total').val(Math.round(t10mg_total));
			$('#t10mon_total').val(Math.round(t10mon_total));

			$('.t5_total_internal_risk').val(Math.round(t5_total_internal_risk));
			$('.t9_total_external_risk').val(Math.round(t9_total_external_risk));
			$('.t10_total_natural_risk').val(Math.round(t10_total_natural_risk));
			$('.overall_risk_rating').val(Math.round(overall_risk_rating));

		});

});